var models = require("./sdk/tools").models;

function formatVendor(v) {

    var result = {
        "PrimaryEmailAddr": {
            "Address": v.email
        },
        "PrimaryPhone": {
            "FreeFormNumber": v.contactNo
        },
        "DisplayName": v.name,
        "Mobile": {
            "FreeFormNumber": v.contactNo
        },
        "AcctNum": v.accountNum,
        "CompanyName": v.name,
        "PrintOnCheckName": v.name
    };


    if (!result.PrimaryEmailAddr.Address)
        delete result.PrimaryEmailAddr;

    return result;
}

function isTypeCoffee(item) {

    let catString = item.category.join(",").toLowerCase();
    return catString.includes("coffee") || catString.includes("notax");
}

async function formatInvoice(v, c, i, a) {

    let lineItemList = [];
    var latestTotal = 0;

    for (var z = v.list.length - 1; z >= 0; z--) {
        var element = v.list[z];

        var item = await i.findOne({ _id: element._id })
            .exec();

        if (!item.qb_id)
            continue

        var list_item = {
            "DetailType": "SalesItemLineDetail",
            "Amount": element.amt * element.unitPrice,
            "SalesItemLineDetail": {
                "ItemRef": {
                    "name": element.sku,
                    "value": item.qb_id
                },
                Qty: element.amt,
                UnitPrice: element.unitPrice
            }
        };

        latestTotal += list_item.Amount;

        if (!isTypeCoffee(element)) {
            //delete list_item.TxnTaxDetail;
            list_item.SalesItemLineDetail.TaxCodeRef = {
                "value": "TAX"
            }

        }

        lineItemList.push(list_item);


    }


    var customer = await c.findOne({ name: v.customer })
        .exec();

    if (!customer || !customer.qb_id)
        return;

    var tax2 = v.tax2 ? v.tax2 : 0
    var rTotal = latestTotal + tax2 + v.taxValue;

    if (v.value > rTotal) {
        v.value = latestTotal;
    }

    //DepositAccount
    var accountDeposit = await a.findOne({
            id: "DepositAccount"
        })
        .exec();

    if(!accountDeposit)
        return;

    var result = {
        "Line": lineItemList,
        "CustomerRef": {
            "value": customer.qb_id,
            name: customer.name
        },
        "DepositToAccountRef": {
            "name": accountDeposit.data.Account.FullyQualifiedName,
            "value": accountDeposit.data.Account.Id
        },
        DocNumber: v.id,
        Deposit: v.value ? v.value : 0,
        "TxnTaxDetail": {}
    };


    // taxrate2 taxRate
    result.TxnTaxDetail.TxnTaxCodeRef = await getTx(v);

    // Set ship address to auto determine invoice taxes
    // Origin address will be company address.
    result.ShipAddr = await getShipAddr(v);

    console.log(result);

    if (!result.ShipAddr)
        delete result.ShipAddr;

    return result;
}

async function getLocation(invoice) {

    var location = await models.location.findOne({
        _id: invoice.location
    }).exec();

    if (!location) {

        location = await models.customer.findOne({
            _id: invoice.location
        }).exec();
    }

    return location
}

async function getShipAddr(invoice) {

    var location = await getLocation(invoice);

    if (!location.address) {
        return null;
    }


    var address = location.address.replace("USA", "")
        .split(",");

    var zip_state = address[2].trim().split(" ")

    return {
        "City": address[1],
        "Line1": address[0],
        "PostalCode": zip_state[1],
        "CountrySubDivisionCode": zip_state[0],
    }

}

async function getTx(invoice) {


    var location = await getLocation(invoice)

    if (!location.taxb2)
        return null

    var rate = await models.taxRate.findOne({
        _id: location.taxb2
    }).exec();

    if (!rate.tax_code)
        return null

    return { value: rate.tax_code };
}


async function formatCustomer(v) {

    if (!v.address) {
        return null;
    }

    var address = v.address.replace("USA", "")
        .split(",");

    var zip_state = address[2].trim().split(" ")

    var entry = {
        "City": address[1],
        "Line1": address[0],
        "PostalCode": zip_state[1],
        "CountrySubDivisionCode": zip_state[0],
    }

    var result = {
        "FullyQualifiedName": v.name,
        "PrimaryEmailAddr": {
            "Address": v.email
        },
        "DisplayName": v.name,
        "Notes": v.notes,
        "FamilyName": v.name,
        "PrimaryPhone": {
            "FreeFormNumber": v.contactNo
        },
        "CompanyName": v.name,
        "BillAddr": entry
    };

    var rate = await models.taxRate.findOne({
        _id: v.taxb2
    }).exec();

    if (rate.tax_code) {
        result["DefaultTaxCodeRef"] = { value: rate.tax_code }
    }

    if (!result.PrimaryEmailAddr.Address || !result.PrimaryEmailAddr.Address.includes("@"))
        delete result.PrimaryEmailAddr;


    return result;
}

function formatEmployee(v) {

    if (!v.name)
        return;

    var name_parts = v.name.trim().split(" ");
    var result = {
        "GivenName": name_parts[0],
        "FamilyName": name_parts.length > 1 ? name_parts[name_parts.length - 1] : "",
        "SSN": v.ssn,
        "PrimaryPhone": {
            "FreeFormNumber": v.contactNo
        }
    };


    return result;
}

function isTypeCoffee(item) {
    if (!item.category)
        return;

    let catString = item.category.join(",").toLowerCase();
    return catString.includes("coffee") || catString.includes("notax");
}


async function formatInventory(i, v, a) {

    var result;

    var vendor = await v.findOne({ name: i.vendor })
        .exec();

    if (!vendor || !vendor.qb_id)
        return;

    var account = await a.findOne({
            id: "Inventory"
        })
        .exec();

    var accountCOGS = await a.findOne({
            id: "Cost of Goods Sold"
        })
        .exec();

    var accountSPI = await a.findOne({
            id: "Sales of Product Income"
        })
        .exec();


    if (!account || !accountCOGS || !accountCOGS.data || !accountSPI)
        return;

    result = {
        "TrackQtyOnHand": true,
        "Name": i.sku,
        "Description": i.itemName,
        "QtyOnHand": i.unit ? i.unit : 0,
        "AssetAccountRef": {
            "name": account.data.Account.FullyQualifiedName,
            "value": account.data.Account.Id
        },
        "InvStartDate": (new Date(i.createdAt)).toISOString().split("T")[0],
        "Type": "Inventory",
        "ExpenseAccountRef": {
            "name": accountCOGS.data.Account.FullyQualifiedName,
            "value": accountCOGS.data.Account.Id
        },
        "IncomeAccountRef": {
            "name": accountSPI.data.Account.FullyQualifiedName,
            "value": accountSPI.data.Account.Id
        },
        "PrefVendorRef": {
            name: vendor.name,
            value: vendor.qb_id
        }
    };

    result.Taxable = !isTypeCoffee(i);
    result.PurchaseCost = i.unitCost;
    result.UnitPrice = i.unitPrice;

    if (i.qb_id) {
        // Prevent overwrite
        delete result.InvStartDate;
    }


    return result;
}

function formatTax(rates) {

}

function sleep(delay = 1) {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(true);
        }, 1000 * delay)
    });

}




module.exports = {
    formatVendor,
    formatInventory,
    formatInvoice,
    formatCustomer,
    formatEmployee,
    sleep
}