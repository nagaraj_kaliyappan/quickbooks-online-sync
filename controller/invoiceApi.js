const models = require("../models/mongo/models/models");
const Invoice = models.invoice;

class invoiceApi {
  constructor() {}

  saveInvoiceDetails(qresponse) {
    return new Promise((resolve, reject) => {
      let invoice = new Array();
      invoice = qresponse.Invoice;
      let dbReq = [];
      invoice.forEach((element, index) => {
        dbReq.push({
          customer: element.CustomerRef.name,
          total: Number(element.TotalAmt),
          id: element.Id,
          value: Number(element.Balance),
          taxRate: Number(0),
          taxValue: Number(element.TxnTaxDetail.TotalTax),
          location:
            element.BillAddr.Line1 +
            "," +
            element.BillAddr.Line2 +
            "," +
            element.BillAddr.Line3 +
            "," +
            element.BillAddr.Line4,
          createdAt: element.MetaData.CreateTime,
        });
        if (index == invoice.length - 1) {
          Invoice.insertMany(dbReq).then(
            (result) => {
              resolve(result);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });
    });
  }

  createORUpdateInvoiceDetails(response, reqId) {
    return new Promise((resolve, reject) => {
      let invoice = response.Invoice;
      if (reqId) {
        Invoice.findOneAndUpdate(
          {
            _id: reqId,
          },
          {
            $set: {
              id: invoice.Id,
              qb_id: invoice.Id,
              customer: invoice.CustomerRef.name,
              total: Number(invoice.TotalAmt),
              value: Number(invoice.Balance),
              taxRate: Number(0),
              taxValue: Number(invoice.TxnTaxDetail.TotalTax),
              location:
                invoice.BillAddr.Line1 +
                "," +
                invoice.BillAddr.Line2 +
                "," +
                invoice.BillAddr.Line3 +
                "," +
                invoice.BillAddr.Line4,
              createdAt: invoice.MetaData.CreateTime,
            },
          }
        ).then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      } else {
        let dbReq = new Invoice({
          id: invoice.Id,
          qb_id: invoice.Id,
          customer: invoice.CustomerRef.name,
          total: Number(invoice.TotalAmt),
          value: Number(invoice.Balance),
          taxRate: Number(0),
          taxValue: Number(invoice.TxnTaxDetail.TotalTax),
          location:
            invoice.BillAddr.Line1 +
            "," +
            invoice.BillAddr.Line2 +
            "," +
            invoice.BillAddr.Line3 +
            "," +
            invoice.BillAddr.Line4,
          createdAt: invoice.MetaData.CreateTime,
        });
        dbReq.save().then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      }
    });
  }

  invoiceList() {
    return new Promise((resolve, reject) => {
      Invoice.find({}).exec((err, invoice) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(invoice);
        }
      });
    });
  }
}

module.exports = invoiceApi;
