const OAuthClient = require("intuit-oauth");
var sdk = require("./sdk/tools");


// https://stackoverflow.com/questions/42755664/capitalize-first-letter-of-each-word-in-js
function capitalizeFirstLetters(str) {
    return str.toLowerCase().replace(/^\w|\s\w/g, function(letter) {
        return letter.toUpperCase();
    })
}

// Parameter e is the resource type name
// ie : /vendor for vednors
async function addRecord(oauthClient, e, body) {


    try {

        var response;
        const companyID = oauthClient.getToken().realmId;
        const url =
            oauthClient.environment == "sandbox" ?
            OAuthClient.environment.sandbox :
            OAuthClient.environment.production;

        response = await oauthClient
            .makeApiCall({
                url: `${url}v3/company/${companyID}${e}?minorversion=55`, ///1234/item
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body),
            })



        return response;

    } catch (e) {
        console.log(e);
        return;
    }
}

// Inserting some data on quickbooks will require
// setting an Id for an account record. This is one 
// example with inventory.
async function syncAccount(oauthClient) {

    var account = await sdk.models.account.findOne({
            id: "Inventory"
        })
        .exec();

    if (!account) {

        var req = {
            "Name": "DHUB_ITEM_ACCOUNT",
            "AccountType": "Other Current Asset",
            "AccountSubType": "Inventory"
        }

        var result = await addRecord(
            oauthClient,
            "/account",
            req
        )

        var account = new sdk.models.account({
            id: "Inventory",
            data: result.json
        });

        await account.save();
    }

}

async function syncDepositAccount(oauthClient) {

    var acId = "DepositAccount";

    var account = await sdk.models.account.findOne({
            id: acId
        })
        .exec();

    if (!account) {

        var req = {
            "Name": "DHUB Deposit Account",
            "AccountType": "Other Current Asset"
        }

        var result = await addRecord(
            oauthClient,
            "/account",
            req
        )

        var account = new sdk.models.account({
            id: acId,
            data: result.json
        });

        await account.save();
    }

}

//Other Current Asset

async function syncSPI(oauthClient) {

    var acId = "Sales of Product Income";

    var account = await sdk.models.account.findOne({
            id: acId
        })
        .exec();

    if (!account) {

        var req = {
            "Name": "dHub Sales Of Product Income",
            "AccountType": "Income",
            "AccountSubType": "SalesOfProductIncome",
        }

        var result = await addRecord(
            oauthClient,
            "/account",
            req
        )

        var account = new sdk.models.account({
            id: acId,
            data: result.json
        });

        await account.save();
    }


}

async function syncCOGS(oauthClient) {

    var acId = "Cost of Goods Sold";
    var account = await sdk.models.account.findOne({
            id: acId
        })
        .exec();

    if (!account) {

        var req = {
            "Name": "DHUB_COGS_ACCOUNT",
            "AccountType": "Cost of Goods Sold",
            "AccountSubType": "SuppliesMaterialsCogs"
        }

        var result = await addRecord(
            oauthClient,
            "/account",
            req
        )

        var account = new sdk.models.account({
            id: "Cost of Goods Sold",
            data: result.json
        });

        await account.save();
    }

}


async function updateRecord(oauthClient, e, data, table) {
    console.log("Updating...");

    let id = data.qb_id + "";

    var response;

    try {
        var result = await getItemBy(id, e, oauthClient);

        delete data.qb_id;

        var item = Object.assign({}, data);
        var qb_data = result.json[table];

        item.Id = qb_data.Id;
        item.SyncToken = qb_data.SyncToken;
        item.sparse = true;

        console.log("Saving item : ", item);
        response = await addRecord(oauthClient, e, item);


    } catch (e) {
        console.log(e);
    }

    return response;
}

async function getItemBy(id, e, oauthClient) {

    const companyID = oauthClient.getToken().realmId;

    const url =
        oauthClient.environment == "sandbox" ?
        OAuthClient.environment.sandbox :
        OAuthClient.environment.production;

    var url_req = `${url}v3/company/${companyID}${e}/${id}?minorversion=55`;

    console.log("Request ", url_req);

    var authResponse = await oauthClient
        .makeApiCall({
            url: url_req,
        });

    return authResponse;

}

async function performRequest(oauthClient, query) {

    const companyID = oauthClient.getToken().realmId;

    const url =
        oauthClient.environment == "sandbox" ?
        OAuthClient.environment.sandbox :
        OAuthClient.environment.production;

    var authResponse = await oauthClient
        .makeApiCall({
            url: `${url}v3/company/${companyID}/query?query=${query}&minorversion=55`,
        });

    return authResponse;
}

function getAuthURL(oauthClient) {
    return oauthClient.authorizeUri({
        scope: [OAuthClient.scopes.Accounting],
        state: "testState",
    });
}

//url title
async function alertUserAuth(url) {

    try {
        await sdk.models.alert.remove({ id: 6000 }).exec();
    } catch (e) {
        console.log(e);
    }

    var data = new sdk.models.alert({
        id: 6000,
        title: "QuickBooks Authentication",
        description: "Click on the button below to log on to your QuickBooks instance.",
        action: {
            title: "Authenticate",
            url: url
        }
    })


    await data.save();

}

async function initializeClient() {


    var pluginSettings = await sdk.getPluginData("QuickBooks Online");

    if (process.argv.indexOf("dev") !== -1) {
        delete pluginSettings.redirect;
        delete pluginSettings.env;
    }

    var oauthClient = new OAuthClient({
        clientId: pluginSettings.clientId ? pluginSettings.clientId : "ABhIRHvKake5fXh7prmhZIZAji7aIFZi7vkxULYbDMjRR8X3sS", // pluginSettings.clientId
        clientSecret: pluginSettings.clientSecret ? pluginSettings.clientSecret : "bGy08fqegdxzLZxSQ3K37TWmBKAgvGjr6OGOyQPR", // pluginSettings.clientSecret
        environment: pluginSettings.env ? pluginSettings.env : "sandbox", // pluginSettings.env
        redirectUri: pluginSettings.redirect && pluginSettings.redirect.length > 0 ? pluginSettings.redirect : "http://localhost:8000/callback", // pluginSettings.redirect
    });

    return oauthClient;
}

async function getTotalNoOfPages(oauthClient, collection, pageSize) {
    let authResponse = await performRequest(
        oauthClient,
        "SELECT COUNT(*) FROM " + collection
    );
    let response = authResponse.json;
    let qresponse = response.QueryResponse;
    let employeeCount = qresponse.totalCount;
    return Math.ceil(employeeCount / pageSize);
}

async function queryBuilder(collection, pageNumber, pageSize) {
    let startPosition = pageNumber === 0 ? 1 : pageNumber * pageSize;
    return `select * from ${collection} STARTPOSITION ${startPosition} MAXRESULTS ${pageSize}`;
}





module.exports = {
    performRequest,
    getAuthURL,
    alertUserAuth,
    getTotalNoOfPages,
    queryBuilder,
    initializeClient,
    addRecord,
    updateRecord,
    syncAccount,
    syncCOGS,
    syncSPI,
    capitalizeFirstLetters,
    syncDepositAccount
}