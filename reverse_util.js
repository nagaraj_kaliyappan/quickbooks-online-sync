const util = require("./util")
const dutil = require("./data_util")
const models = require("./sdk/tools").models;
var pageSize = 30
var delay = 7


async function fetchData(client, res_name, page) {

    console.log(res_name, page)

    try {

        var query = await util.queryBuilder(res_name, parseInt(page), pageSize);
        var res = await util.performRequest(client, query);

        var resName = util.capitalizeFirstLetters(res_name);
        var keys = Object.keys(res.json.QueryResponse)

        return res.json.QueryResponse[keys[0]]

    } catch (e) {
        console.log(e)

        return null
    }


}

async function totalPage(client, res_name) {

    var res = await util.getTotalNoOfPages(client, res_name, pageSize)

    return res
}


async function merge(set, model_name, match, matchWithSet, upsert = false) {

    for (var i = set.length - 1; i >= 0; i--) {
        var row = set[i];

        var query = {};
        query[match] = row[matchWithSet]

        console.log(query)


        await models[model_name].findOneAndUpdate(query, {
            $set: {
                qb_id: row.Id
            }
        }, { upsert }).exec()

    }

}

async function get_vendor(client) {

    var page_req = await client.get("page_count/vendor");

    for (var i = page_req.data.count - 1; i >= 0; i--) {

        var res = await client.get("download/vendor?page=" + i)

        await merge(res.data, "vendor", "name", "DisplayName")
        if (i > 0)
            dutil.sleep(delay)

    }

}

async function get_item(client) {

    var page_req = await client.get("page_count/item");

    for (var i = page_req.data.count - 1; i >= 0; i--) {

        var res = await client.get("download/item?page=" + i)
        await merge(res.data, "item", "itemName", "Name")

        if (i > 0)
            dutil.sleep(delay)
    }

}

async function get_taxrate(client) {
    var page_req = await client.get("page_count/TaxRate");

    for (var i = page_req.data.count - 1; i >= 0; i--) {

        var res = await client.get("download/TaxRate?page=" + i)
        await merge(res.data, "taxRate", "name", "Name")

        if (i > 0)
            dutil.sleep(delay)
    }
}

async function get_customer(client) {

    var page_req = await client.get("page_count/customer");

    for (var i = page_req.data.count - 1; i >= 0; i--) {

        var res = await client.get("download/customer?page=" + i)
        await merge(res.data, "customer", "name", "DisplayName")

        if (i > 0)
            dutil.sleep(delay)
    }

}


async function get_employee(client) {

    var page_req = await client.get("page_count/employee");

    for (var i = page_req.data.count - 1; i >= 0; i--) {

        var res = await client.get("download/employee?page=" + i)
        console.log(res.data)
    }

}

async function get_invoice(client) {

    var page_req = await client.get("page_count/invoice");

    for (var i = page_req.data.count - 1; i >= 0; i--) {

        var res = await client.get("download/invoice?page=" + i)
        console.log(res.data[0])
    }

}

async function get_taxag(client) {

    var page_req = await client.get("page_count/TaxAgency");

    for (var i = page_req.data.count - 1; i >= 0; i--) {
        var res = await client.get("download/TaxAgency?page=" + i)
        await merge(res.data, "taxAg", "id", "DisplayName", true)
        if (i > 0)
            dutil.sleep(delay)
    }

}

async function get_accounts(client) {

    var page_req = await client.get("page_count/account");
    var result = []

    for (var i = page_req.data.count - 1; i >= 0; i--) {
        var res = await client.get("download/account?page=" + i)

        result = result.concat(res.data)
        if (i > 0)
            dutil.sleep(delay)
    }

    return result
}

async function get_cogs(client, accounts) {

    var init = { "id": "Cost of Goods Sold" }
    var account = accounts.filter(el => { return el.Name == "DHUB_COGS_ACCOUNT" })[0]
    console.log(account)

    if(!account)
    	return;

    var res = await models.account.findOneAndUpdate(init, {
        $set: {
            qb_id: account.Id,
            data: {
                Account: account
            }
        }
    }, { upsert: true }).exec()

}

async function get_itemAccount(client, accounts) {

    var init = { "id": "Inventory" }
    var account = accounts.filter(el => { return el.Name == "DHUB_ITEM_ACCOUNT" })[0]
    console.log(account)

    if(!account)
    	return;

    var res = await models.account.findOneAndUpdate(init, {
        $set: {
            qb_id: account.Id,
            data: {
                Account: account
            }
        }
    }, { upsert: true }).exec()



}

async function get_sopi(client, accounts) {

    var init = { "id": "Sales of Product Income" }
    var account = accounts.filter(el => { return el.Name == "dHub Sales Of Product Income" })[0]
    console.log(account)

    if(!account)
    	return;

    var res = await models.account.findOneAndUpdate(init, {
        $set: {
            qb_id: account.Id,
            data: {
                Account: account
            }
        }
    }, { upsert: true }).exec()

}


module.exports = {
    fetchData,
    totalPage,
    get_vendor,
    get_item,
    get_invoice,
    get_employee,
    get_customer,
    get_cogs,
    get_itemAccount,
    get_sopi,
    get_taxag,
    get_accounts,
    get_taxrate
}