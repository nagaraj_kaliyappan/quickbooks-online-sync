let util = require("./util");

var oauthClient;
var authToken = {
         realmId: '9130349287337796',
         token_type: 'bearer',
         access_token: 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..RuMNl3dufzGP5Z9GOS426A.vhd5P2qflQVr0JeaGjOLSMpfr06WEqdnf6gWWmWVh3gLQn-x3WHrcL9LPhvPS5Q9sW5Ko6nt5dF1KBoGN7E3c0ZXNgxK_QkAfdVGW35ls7KTov3Gh-zGojVnq3Xy1sCZvAzKS2qRJzT1dU6N74wQyk2N5XNqT8ijH2qoaEIBFqVPlNbNwrHjup5rSQb7LHwG1Rbu3Dnxx1vhKarPTN98A4J6qvsV6ZXGO3uEk3CDZAmK-iDb8OhNepuofiZmlKAPhrh-x8jdENZ3i60gfvjLGn-d0XRBDFZwu2t7NWvUxzFUcTUl272SBrE3HDR6G9AzVxXbOPYUdYTeszEGyGsCjVWoOPFj0qOMTG7R_sCrlxHR0sCzGQZqM6pqdhUPy1PdD4K0GQAcSV7DnkV_bZov0n18ZYCVeClUQK3Thgs-aIcsqGLkswuiR5s1z8ipp_LNgICFtxb9TVfXvAbj1TgRsu8bkKMOrCJZ7mO2ALEthSraVlBknSGcw9n5XQPM8pwJoSsTNNj3KQFsSKaR7Cgy7QvRZeNd7gaUJtUJUvVDPXunn8TmylmNzupq5_L5CKzRHmnLCBDULF28NRzMyG6yj8QhmwDK-dgQeEyC5dU5rDjPdQXSKbFQQlftYYwjzj9byaCH8Kap2XN40lyQZ4WUxeKjgxG50BLQFGSTNAzcltbVqEccaHTTsvumeH9klv3prN1vwaqVsfTYW_i2fkW9N5I712v3zQjAb752iJeHjWY.4n5qodHBSFujWQoi70fIjg',
         refresh_token: 'AB11613538487vCmLqmruMzwAI07gP6t6l8hGW5hbX0FMQbc0j',
         expires_in: 3600,
         x_refresh_token_expires_in: 8695092,
         id_token: '',
         latency: 60000,
         createdAt: 1604843395857,
         state: 'testState' }

util.initializeClient().then( client => {
  oauthClient = client;
  
  oauthClient.setToken(authToken);

  util.performRequest(oauthClient, "select * from Account WHERE Id = '157'")
  .then(res => {
    console.log(res.json);
  })
});

/*
{ "_id" : ObjectId("5fa77d3770c5632b3f716ebb"), "id" : "Cost of Goods Sold", "data" : { "Account" : { "Name" : "DHUB_COGS_ACCOUNT", "SubAccount" : false, "FullyQualifiedName" : "DHUB_COGS_ACCOUNT", "Active" : true, "Classification" : "Expense", "AccountType" : "Cost of Goods Sold", "AccountSubType" : "CostOfSales", "CurrentBalance" : 0, "CurrentBalanceWithSubAccounts" : 0, "CurrencyRef" : { "value" : "USD", "name" : "United States Dollar" }, "domain" : "QBO", "sparse" : false, "Id" : "157", "SyncToken" : "0", "MetaData" : { "CreateTime" : "2020-11-07T21:08:07-08:00", "LastUpdatedTime" : "2020-11-07T21:08:07-08:00" } }, "time" : "2020-11-07T21:08:07.481-08:00" }, "__v" : 0 }
*/