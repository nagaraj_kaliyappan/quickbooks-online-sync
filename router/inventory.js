let express = require('express');
let router = express.Router();
let inventory = require('../controller/inventoryApi');

let inventoryApi = new inventory();
router.get('/inventory_list', function(req, res) {
    inventoryApi.inventoryList().then((result)=>{
        res.send(result);
    },error=>{
        res.status(500).send(error);
    });
});



module.exports = router;