let express = require('express');
let router = express.Router();
let invoice = require('../controller/invoiceApi');

let invoiceApi = new invoice();

router.get('/invoice_list', function(req, res) {
    invoiceApi.invoiceList().then((result)=>{
        res.send(result);
    },error=>{
        res.status(500).send(error);
    });
});





module.exports = router;