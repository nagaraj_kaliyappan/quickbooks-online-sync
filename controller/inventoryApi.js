const models = require("../models/mongo/models/models");
const Inventory = models.item;

class inventoryApi {
  constructor() {}

  saveInventoryDetails(qresponse) {
    return new Promise((resolve, reject) => {
      let item = new Array();
      item = qresponse.Item;
      let dbReq = [];
      item.forEach((element, index) => {
        dbReq.push({
          id: element.Id,
          itemName: element.Name,
          unitPrice: Number(element.UnitPrice),
          category: [element.Type],
          amt: Number(element.IncomeAccountRef.value),
          createdAt: element.MetaData.CreateTime,
        });
        if (index == item.length - 1) {
          Inventory.insertMany(dbReq).then(
            (result) => {
              resolve(result);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });
    });
  }

  createORUpdateInventoryDetails(response, reqId) {
    return new Promise((resolve, reject) => {
      let inventory = response.Item;
      if (reqId) {
        Inventory.findOneAndUpdate(
          {
            _id: reqId,
          },
          {
            $set: {
              id: inventory.Id,
              qb_id: inventory.Id,
              itemName: inventory.Name,
              unitPrice: Number(inventory.UnitPrice),
              category: [inventory.Type],
              amt: Number(inventory.IncomeAccountRef.value),
              createdAt: inventory.MetaData.CreateTime,
            },
          }
        ).then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      } else {
        let dbReq = new Inventory({
          id: inventory.Id,
          qb_id: inventory.Id,
          itemName: inventory.Name,
          unitPrice: Number(inventory.UnitPrice),
          category: [inventory.Type],
          amt: Number(inventory.IncomeAccountRef.value),
          createdAt: inventory.MetaData.CreateTime,
        });
        dbReq.save().then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      }
    });
  }

  inventoryList() {
    return new Promise((resolve, reject) => {
      Inventory.find({}).exec((err, inventory) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(inventory);
        }
      });
    });
  }
}

module.exports = inventoryApi;
