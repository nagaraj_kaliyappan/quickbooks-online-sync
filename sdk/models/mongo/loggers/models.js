let Logger = require("../../../controller/v1/logger");
let models = require("../models/models");
var NodeGeocoder = require('node-geocoder');
let makeForm = require("../../../po_form");
let config = require("../../../controller/config");
 
var options = {
  provider: 'google',
 
  // Optional depending on the providers
  httpAdapter: 'https', // Default
  apiKey: 'AIzaSyDLgmwUaIeLVE6XBhaTo6LVBXZFG3CtLcE', // for Mapquest, OpenCage, Google Premier
  formatter: null         // 'gpx', 'string', ...
};
 
var geocoder = NodeGeocoder(options);


let logFunctions = {
    item: {
        post: (req, name) => {

            if (!req.body.category || req.body.category.indexOf("MACHINE") == -1)
                Logger.log(req, "New Product", `${req.body.itemName} added to database.`);

            //propegateVendor(req.body, true);
        },
        put: (req, name) => {

            let newPrice = req.body.unitPrice;

            //propegateVendor(req.body);

            models.item.findOne({ name: name })
                .exec(
                    (err, item) => {
                        if (err) {
                            console.log(err);
                            return;
                        }

                        if (req.body.category && req.body.category.indexOf("MACHINE") != -1) {
                            let requestParts = req.body.parts;
                            if (!item.parts || items.parts.join(",") != requestParts.join(",")) {
                                Logger.log(req, `${name} service log.`, `New parts added to machine: ${requestParts.join(",")} `);
                            }
                        }

                        if (newPrice) {

                            if (newPrice && newPrice > item.unitPrice) {

                                alertSys(
                                    `${item.itemName} price has increased`,
                                    `${item.itemName} now priced at ${newPrice.toFixed(2) } USD`
                                );
                            }

                            if (newPrice && newPrice.toFixed && newPrice < item.unitPrice) {
                                alertSys(
                                    `${item.itemName} price has decreased`,
                                    `${item.itemName} now priced at ${newPrice.toFixed(2) } USD`
                                );
                            }
                        }

                    });


        }
    },
    loss: {
        post: (req, name) => {
            let loss = req.body;

            if (loss.reason) {
                models.item.findOne({
                    itemName: loss.itemName,
                    unitCost: loss.itemPrice
                }).exec((err, item) => {
                    if (err) {
                        console.log(err);
                        return;
                    }

                    checkIfBelow(item, loss.amount);
                });

            }
        }
    },
    invoice: {
        delete: (req) => {
            let invoiceId = req.params.id;

            models.invoice.findOne({ _id: invoiceId })
                .exec((err, invoice) => {
                    if (err || !invoice) {
                        console.log(err || { error: "Invoice not found." });
                        return;
                    }

                    if (invoice.list) {
                        for (var i = invoice.list.length - 1; i >= 0; i--) {
                            let item = invoice.list[i];
                            addUnit(item);
                        }
                    }

                })
        },
        post: (req, name) => {
            let invoice = req.body;
            let owner = req.owner;
            name = req.name;

            if(!req.body.salesman)
                req.body.salesman = req.owner;


            if (invoice.list) {
                for (var i = invoice.list.length - 1; i >= 0; i--) {
                    let item = invoice.list[i];



                    if (item.touched) {

                        alertSys(
                            `${name} has updated a product price`,
                            `${name} has changed product ${item.itemName} price to ${item.unitPrice.toFixed(2) } USD`
                        );

                        Logger.log(
                            req,
                            `${name}— Price updates`,
                            `Updated ${item.itemName} price to ${item.unitPrice.toFixed(2) } USD`);
                    }

                    var amt = item.amt;

                    if(item.rel){
                      amt = amt * item.rel.amt;
                    }

                    checkIfBelow(item, amt);
                    // rel
                    reduceUnit(item, invoice.id, invoice.warehouse,amt);


                }
                determineOrderPriceChange(name, invoice);
            }

            Logger.log(
                req,
                `${name}—Orders`,
                `Placed order for ${invoice.total.toFixed(2) } USD with ${invoice.customer}`);
        }
    },
    purchaseOrder : {
        post : (req) => {

            if(req.body.ppo){


                models.vendor.findOne({ name : req.body.vendor }, (err, vendor) => {
                  if(err){
                    console.log(err);
                    return;
                  }

                  makeForm(config, vendor, req.body)
                  .then( fileId => {


            var text = `Hi ${vendor.repName},
${config.displayName} has requested a Purchase order. The form is attached below.

${config.displayName}.
Phone: ${config.phone}`;

                     var file = fs.readFileSync(fileId);
           
                      var data = {
                        from: config.mailgunFrom,
                        to: vendor.email,
                        subject: `${config.displayName} PO # ${req.body.id}`,
                        text: text,
                        attachment: file
                      };
                       
                      config.mailgun.messages().send(data, function (err, body) {
                          if (err) {
                              console.log(err);
                              reject(err);
                              return;
                          }
                          resolve({});
                      });

                  });
               
                });

            }


            if(req.body.list){

                for (var i = req.body.list.length - 1; i >= 0; i--) {
                    var item = req.body.list[i];
                    item.unit = item.amt;
                    propegateVendor(item);
                }
            }
        },
        put : (req) => {


           if(req.body.list && req.body.incomplete){

                for (var i = req.body.list.length - 1; i >= 0; i--) {
                    var item = req.body.list[i];
                    item.amt = item.scanned ? item.scanned : 0;
                    req.body.list[i] = item;
                }

               models.purchaseOrder.findOneAndUpdate( 
                   { _id : req.body._id },
                   { $set : 
                   {
                       list : req.body.list
                   }
               }, (err , success) => {
                   if(err)
                       console.log(err);
               });
           }

        }
    },
    customer : {
        put : (req, name) => {

            // address
            var _id = req.body._id; 
           geocoder.geocode(req.body.address,(err, res) => {

              if(err){
                  console.log(err);
                  return;
              }

              if(!res[0]){
                  console.log("Error -> ", customer.name);
                  return;
              }

             var geometry = {
                    location : {
                        lat : res[0].latitude,
                        lng : res[0].longitude
                    }
            };

            models.customer.findOneAndUpdate({
                     _id : _id
               },{
                  $set : {
                      geometry : geometry
                  }
               }).exec((err) => {
                       if (err) {
                           console.log(err);
                           return;
                       }
                      
              });
         });

        }
    },
    employee: {
        put: (req, name) => {

            let desiredStatus = req.body.status;

            models.employee.findOne({ name: name })
                .exec((err, employee) => {
                    if (err) {
                        console.log(err);
                        return;
                    }

                    if(!employee){
                        console.log("Employee not found");
                        return;
                    }

                    if (employee.status != desiredStatus) {

                        var message, title;
                        switch (desiredStatus) {
                            case "active":
                                title = `${name} has changed their status to active.`;
                                message = "Employee has changed their status to active.";
                                alertSys(title, message);
                                break;

                            default:
                                title = `${name} has changed their status to inactive.`;
                                message = "Employee has changed their status to inactive.";
                                alertSys(title, message);
                                break;
                        }
                        Logger.log(req, title, message)
                    }
                })

        }
    }
}


function findUnit(item, name){
    var result = false;

    if(item.unit_counts && name)
        for (var i = item.unit_counts.length - 1; i >= 0; i--) {
           var u = item.unit_counts[i];
           if(u.name == name){
               result = u;
               break;
           }
        }

    return result;
}

function reducePods(item, id, warehouse, amt = 0){

    models.costLevel.find({ item : item._id })
    .exec((err, costLevels) => {
        if(err){
            console.log(err);
            return;
        }

        var amount =  amt;

        costLevels.sort(function(a, b) {
            return parseFloat(a.checkinDate) - parseFloat(b.checkinDate);
        });

        costLevels.sort(function(a, b) {
            return b.rel ? findUnit(items, b.rel).amt * b.amt : b.amt  -  a.rel ? findUnit(item, a.rel).amt * a.amt : a.amt;
        });
        
        for (var i = 0; i < costLevels.length;i++) {
            var costLevel = costLevels[i],
             podAmount = 0,
             amountPrior = amount + 0;

             var unit = findUnit(item, costLevel.rel).amt;

             var amountInPod = costLevel.amt;

             if(!warehouse)
                 warehouse = costLevel.warehouse;

             if(warehouse !== costLevel.warehouse)
                 continue; 

            if(amount == 0){
                break;
            }

            if(item.rel){
                if(!costLevel.rel){
                    continue;
                }

                if(costLevel.rel.name !== item.rel.name){
                    continue;
                }
            }


               var amountSold = costLevel.amt > amount ? amount : costLevel.amt; 
              
               var d1,d2,d3;

               if(!unit){
                   if(amountInPod > amount){

                       podAmount = amountInPod - amount;
                       amount = 0;
                      
             
                   } else {
                       amount -= costLevel.amt;
                     
                   }
               } else {
                   d1 = Math.floor(amount/unit.amt);
                   d2 = Math.floor(d1 * unit.amt);
                   d3 = costLevel.amt * unit.amt;

                   if(d1 == 0 || d2 == 0)
                       continue;

                    if(d3 > amount){

                       podAmount = costLevel.amt - d1;
                       amount = 0;
                      
             
                   } else {

                       amount -= d3;
                     
                   }

               }

              models.backup.create({
                    invoice : id,
                    amt : unit ? podAmount == 0 ? d3/unit.amt : d1 : amountPrior - amount,
                    location : costLevel.pod,
                    item : item._id,
                    mod_rel : costLevel.rel
              }, (err) => {

                  if(err){
                      console.log(err);
                  }
              });


               models.costLevel.findOneAndUpdate({
                       _id : costLevel._id
               },{
                  $set : {
                    amt : podAmount,
                    sold : costLevel.sold ? costLevel.sold + amountSold : amountSold 
                  }
               }).exec((err) => {
                       if (err) {
                           console.log(err);
                           return;
                       }
                      
              });

        }

        if(amount > 0){
            alertSys(
                "Error processing invoice",
                `Not enough products could be assigned for invoice # ${id}. Item : ${item.itemName}, Missing ${amount}`
            );

        }
    })

}

function reduceUnit(item, id, warehouse, amt = 0) {
    if (!item.amt) {
        return;
    }

    reducePods(item, id, warehouse, amt);
    models.item.update({
            _id: item._id
        }, { $inc: { unit: -1 * amt } })
        .exec(err => {
            if (err) {
                console.log("Error reducing unit", err);
            }
        })
}

function addUnit(item) {
    if (!item.amt) {
        return;
    }

    models.item.update({
            _id: item._id
        }, { $inc: { unit: item.amt } })
        .exec(err => {
            if (err) {
                console.log("Error reducing unit", err);
            }
        })
}

function determineOrderPriceChange(name, invoice) {
    for (var i = invoice.list.length - 1; i >= 0; i--) {
        if (invoice.list[i].touched) {
            let item = invoice.list[i];

            compareToCurrentPrice(name, item);

        }
    }
}

function compareToCurrentPrice(name, item) {
    models.item.findOne({ _id: item._id })
        .exec(
            (err, currentItem) => {
                if (err) {
                    console.log(err);
                    return;
                }

                if (item.unitPrice > currentItem.unitPrice) {
                    alertSys(
                        `${name} has increased a product price`,
                        `${name} has changed product ${item.itemName} price from ${currentItem.unitPrice.toFixed(2)} to ${item.unitPrice.toFixed(2) } USD`
                    );
                }

                if (item.unitPrice < currentItem.unitPrice) {

                    alertSys(
                        `${name} has decreased a product price`,
                        `${name} has changed product ${item.itemName} price from ${currentItem.unitPrice.toFixed(2)} to ${item.unitPrice.toFixed(2) } USD`
                    );
                }
            }
        )
}

function checkIfBelow(item, desiredAmount) {
    if (item.alertBelow) {
        let alertBelow = parseInt(item.alertBelow);

        if ((item.unit - desiredAmount) < alertBelow) {
            alertSys(
                `${item.itemName} is below desired inventory amount.`,
                `You have ${item.unit} units left`
            );
        }
    }
}

function propegateVendor(item, newProduct){

    if(!item.vendor || !item.unit){
        return;
    }

    models.vendor.update(
        { name: item.vendor },
        { $set: 
            { lastPurchase : new Date() } ,
          $inc : {
              stock : item.unit
          }
        }, 
        { multi: true }).exec()
        .then(function(data) {
              
        })
        .catch(function(error) {
                console.log(error);
     });

    if(newProduct){
        var unitCost = item.unitCost;
        var units = item.unit;
        addToVendorHistory(item.vendor, unitCost, units);    
    }
}

function addToVendorHistory(name, unitCost, units){
     models.vendor.update({ name : name },
        { $push: 
            {   
                history : {
                    unitCost,
                    units,
                    date : new Date()
                } 
            } 
        }, 
        { multi: true }).exec()
        .then(function(data) {
              
        })
        .catch(function(error) {
                console.log(error);
         });
}

function alertSys(title, message) {
    let alert = new models.alert({
        title: title,
        message: message
    });

    alert.save((err) => {
        if (err) {
            console.log(err);
        }
    })
}

logFunctions.addToVendorHistory = addToVendorHistory;

module.exports = logFunctions;