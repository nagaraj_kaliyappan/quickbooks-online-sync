
// Mailgun api key
var api_key = '';

// Mailgun domain
var domain = '';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
const config = {
	name : "",
	displayName : "",
	phone : "",
	contactName : "",
	address : '',
	header : `
		Name
		Address
		Contact Info
	`,
	email : {
	    host: '',
	    port: 587,
	    auth: {
	        user: "",
	        pass: ""
	    }
	},
	mailgun : mailgun,
	mailgunFrom : "",
	logo : jtLogo,
	emailAddress : "",
	dba : "TESTDB",
	defaultPw : "",
	signUpEmail : "",
	pagination : {
		pageSize : 10
	}
};

module.exports = config;
