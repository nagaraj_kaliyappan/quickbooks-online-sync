let mongoose = require("mongoose");
mongoose.Promise = global.Promise;

let mongoConnection = require("../connection/mongoConnection").connection;

var dbName = "testDB", config;

if(process.argv.indexOf("dev") == -1){
    config = require("../../../../../controller/config");
    dbName = config.dba;
}

let jvtDb = mongoConnection.useDb(dbName);

let userSchema = mongoose.Schema({
    username: {type: String, unique: true, required: true},
    password: String,
    email: String,
    mobile: {type: String},
    signUpIpAddress: String,
    lastLoginIpAddress: [String],
    createdById: String,
    status: String,
    name : String,
    lastLogin: Date,
    createdAt: Date,
    updatedAt: [Date],
    isDeleted: {type: Boolean, default: false}
}, {strict: false});

// barcodes amt name
let unitSchema = mongoose.Schema({
    barcodes : [String],
    amt : Number,
    name : String
}, {strict: false});


let truckSchema = mongoose.Schema({
    no: Number,
    name: String,
    createdAt: Date,
    isDeleted: {type: Boolean, default: false}
}, {strict: false});


let truckLocationSchema = mongoose.Schema({
    lat : Number,
    long : Number,
    name: String,
    route : String,
    createdAt: Date,
    area : Number,
    weight : Number,
    isDeleted: {type: Boolean, default: false}
}, {strict: false});

let routeSchema = mongoose.Schema({
    id: String,
    name: String,
    stops: [String],
    createdAt: Date,
    chargeRate : Number,
    truck : String,
    totalArea : Number,
    totalWeight : Number,
    truckName : String,
    employees : String,
    uiid : String,
    isDeleted: {type: Boolean, default: false}
}, {strict: false});

let warehouseSchema = mongoose.Schema({
    no: Number,
    name: String,
    location: String,
    createdAt: Date,
    isDeleted: {type: Boolean, default: false}
}, {strict: false});

let employeeSchema = mongoose.Schema({
    id: String,
    name: String,
    email: String,
    type: [String],
    truck: String,
    route: Number,
    warehouse: String,
    customers: [String],
    userId: String,
    status: String,
    createdAt: Date,
    qb_id : String,
    isDeleted: {type: Boolean, default: false}
}, {strict: false});

let vendorSchema = mongoose.Schema({
    id: String,
    name: String,
    repName: String,
    contactNo: String,
    email: String,
    lastPurchase : Date,
    accountsPayable : Number,
    status: String,
    createdAt: Date,
    isDeleted: {type: Boolean, default: false},
    qb_id : String,
    accountNum : String
}, {strict: false});

let customerSchema = mongoose.Schema({
    id: String,
    name: String,
    contactNo: String,
    email: String,
    userId: String,
    address : String,
    status: String,
    value : Number,
    taxrate_name : String,
    taxrate2_name : String,
    locations : [String],
    assignedEmployee: [String],
    createdAt: Date,
    fuel_charge : Boolean,
    geometry : mongoose.Schema.Types.Mixed,
    isDeleted: {type: Boolean, default: false},
    qb_id : String,
    taxb1 : String,
    taxb2 : String
}, {strict: false});

let customerPriceSchema = mongoose.Schema({
    customerId: String,
    price: String,
});

let inventorySchema = mongoose.Schema({
    id: String,
    sku: String,
    vendor: String,
    userId: String,
    truck: String,
    warehouse: String,
    unit: Number,
    unitCost: Number,
    itemName : String,
    unitPrice: Number,
    locations: [String],
    category: [String],
    alertBelow: Number,
    amt : Number,
    barcode : [String],
    unit_counts : [unitSchema],
  //  customerPrice: customerPriceSchema,
    createdAt: Date,
    isDeleted: {type: Boolean, default: false},
    lowLevel : Number,
    topLevel : Number,
    frequency : String,
    handQuantity : Number,
    qb_id : String
}, {strict: false});



let locationSchema = mongoose.Schema({
    id : String,
    name: String,
    address: String,
    employees: [String],
    route: [String],
    stops: [String],
    category: String,
    taxrate2_name : String,
    createdAt: Date,
    customer : String,
    fuel_charge : Boolean,
    area : Number,
    weight : Number,
    geometry : mongoose.Schema.Types.Mixed,
    isDeleted: {type: Boolean, default: false},
    taxb1 : String,
    taxb2 : String
}, {strict: false});

let taxRateSchema = mongoose.Schema({
    id: String,
    name: String,
    zip: String,
    percentage: Number,
    customers: [String],
    subrate : Boolean,
    createdAt: Date,
    isDeleted: {type: Boolean, default: false},
    qb_id : String,
    parent : String,
    previous : Number,
    tax_code : String
}, {strict: false});

let alertSchema = mongoose.Schema({
    id: Number,
    title: String,
    description: String,
    createdAt: Date,
    action : mongoose.Schema.Types.Mixed
}, {strict: false});

let accountQBSchema = mongoose.Schema({
    id: String,
    data : mongoose.Schema.Types.Mixed
}, {strict: false});

let entrySchema = mongoose.Schema({
    log : String,
    device : String,
    createdAt: Date
}, {strict: false});

let saleSchema = mongoose.Schema({
    id : String,
    manager : String,
    delivery : Boolean,
    createdAt: Date
}, {strict: false});

let logSchema = mongoose.Schema({
    name : String,
    entries : [entrySchema],
    latestActivity : Date,
    
    createdAt: Date
}, {strict: false});

let soldItemSchema = mongoose.Schema({
    id: String,
    sku: String,
    vendor: String,
    unit: Number,
    unitCost: Number,
    itemName : String,
    unitPrice: Number,
    locations: [String],
    category: [String],
    barcode : [String],
    createdAt: Date,
    scanned : Number,
    amt : Number,
    delivered : Number,
    qb_id : String
}, {strict: false});


let invoiceSchema = mongoose.Schema({
    customer : String,
    list : [soldItemSchema],
    total : Number,
    id : String,
    uiid : String,
    value : Number,
    taxRate : Number,
    taxValue : Number,
    tax2 : Number,
    route : Array,
    owner : String,
    complete : Boolean,
    picked : Boolean,
    location : String,
    ship_label : String,
    weight : Number,
    cube : Number,
    halted : Boolean,
    stop_no : Number,
    truck : String,
    history  : Array, // Payment history 
    createdAt: Date,
    qb_id : String
}, {strict: false});

let purchaseOrderSchema = mongoose.Schema({
    vendor : String,
    list : [soldItemSchema],
    total : Number,
    id : String,
    value : Number,
    taxRate : Number,
    taxValue : Number,
    destination : String,
    ppo : Boolean,
    location : String,
     qb_id : String
}, {strict: false});

let returnSchema = mongoose.Schema({
    item : String,
    cost : Number,
    warehouse : String,
    barcode :[String],
    invoice : String,
    amt : Number,
    id : String,
    scanned : Number,
    reason : String
}, {strict: false});

let costLevelSchema = mongoose.Schema({
    checkinDate : Number,
    item : String,
    amt : Number,
    cost : Number,
    warehouse : String,
    pod: String,
    bin : String,
    barcode : String,
    rel : String
}, {strict: false});


let podSchema = mongoose.Schema({
   l : Number,
   w : Number,
   h : Number,
   id : String,
   warehouse : String,
   aisle : String,
   bay : String,
   bin : String,
   lowLevel : Number,
   topLevel : Number,
   frequency : String,
   handQuantity : Number,
   lock : String,
   stock : Boolean
}, {strict: false});



let ruleSchema = mongoose.Schema({
   percentage : Boolean,
   min : Number,
   name : String,
   products : [String]
}, {strict: false});

let lossSchema = mongoose.Schema({
    amount : Number,
    itemName : String,
    itemPrice : Number,
    reason : String,
    total : Number,
    createdAt: Date
}, {strict: false});

let accountSchema = mongoose.Schema({
    value : Number,
    owner : String,
    createdAt: Date
}, {strict: false});

let backupSchema = mongoose.Schema({
    owner : String,
    //backup : Mixed,
    createdAt: Date,
    interest : Number,
    delay : Number,
    max : Number
}, {strict: false});

let routeLockSchema = mongoose.Schema({
    owner : String,
    locationId : String,
    createdAt: Date
}, {strict: false});


let pluginSchema = mongoose.Schema({
    name : String,
    settings : Array,
    data : mongoose.Schema.Types.Mixed,
    folderId : String,
    lastExec : Number
}, { strict : false});

let referenceSchema = mongoose.Schema({
    id : String,
    data : mongoose.Schema.Types.Mixed,
}, { strict : false});

let boxSchema = mongoose.Schema({
    id : String,
    barcode : String,
    length : Number,
    width : Number,
    height : Number,
    quantity : Number
}, { strict : false});

let zoneSchema = mongoose.Schema({
    id : String,
    barcode : String,
    max : Number,
    warehouse : String
}, { strict : false});

let taxAgSchema = mongoose.Schema({
    qb_id : String,
    id : String
}, { strict : false});


let userModel = jvtDb.model("user", userSchema, "user");
let truckModel = jvtDb.model("truck", truckSchema, "truck");
let truckLocation = jvtDb.model("truckLocation", truckLocationSchema, "truckLocation");
let routeModel = jvtDb.model("route", routeSchema, "route");
let warehouseModel = jvtDb.model("warehouse", warehouseSchema, "warehouse");
let employeeModel = jvtDb.model("employee", employeeSchema, "employee");
let vendorModel = jvtDb.model("vendor", vendorSchema, "vendor");
let customerModel = jvtDb.model("customer", customerSchema, "customer");
let inventoryModel = jvtDb.model("item", inventorySchema, "item");
let locationModel = jvtDb.model("location", locationSchema, "location");
let taxRateModel = jvtDb.model("taxRate", taxRateSchema, "taxRate");
let alertModel = jvtDb.model("alert", alertSchema, "alert");

let accountModel = jvtDb.model("account_qb", accountQBSchema, "account_qb");

let taxAgModel = jvtDb.model("tax_ag", taxAgSchema, "tax_ag");

let logModel = jvtDb.model("log", logSchema, "log");
let saleModel = jvtDb.model("sale", saleSchema, "sale");
let invoiceModel = jvtDb.model("invoice", invoiceSchema, "invoice");

let creditMemoModel = jvtDb.model("creditMemo", invoiceSchema, "creditMemo");

let lossModel = jvtDb.model("loss", lossSchema, "loss");

let routeLockModel = jvtDb.model("routeLock", routeLockSchema, "routeLock");
let backupModel = jvtDb.model("backup", backupSchema, "backup");

let boxModel = jvtDb.model("package", boxSchema, "package");

let zoneModel = jvtDb.model("zone", zoneSchema, "zone");

let refModel = jvtDb.model("reference", referenceSchema, "reference");


let purchaseOrderModel = jvtDb.model("purchaseOrder", purchaseOrderSchema, "purchaseOrder");
let pendingOrderModel = jvtDb.model("pendingPurchaseOrder", purchaseOrderSchema, "pendingPurchaseOrder");
let returnModel = jvtDb.model("return", returnSchema, "return");
let podModel =  jvtDb.model("pod", podSchema, "pod");
let ruleModel = jvtDb.model("rule", ruleSchema, "rule");
let costLevel = jvtDb.model("costLevel",costLevelSchema, "costLevel")

let plugin = jvtDb.model("plugin", pluginSchema, "plugin")


const Models = {
    user: userModel,
    truck: truckModel,
    route: routeModel,
    warehouse: warehouseModel,
    employee: employeeModel,
    vendor: vendorModel,
    customer: customerModel,
    item: inventoryModel,
    inventory : inventoryModel,
    location: locationModel,
    taxRate: taxRateModel,
    alert : alertModel,
    log : logModel,
    sale : saleModel,
    invoice : invoiceModel,
    loss : lossModel,
    routeLock : routeLockModel,
    backup : backupModel,
    purchaseOrder : purchaseOrderModel,
    return : returnModel,
    pod  : podModel,
    rule : ruleModel,
    costLevel : costLevel,
    truckLocation : truckLocation,
    ppo : pendingOrderModel,
    plugin : plugin,
    package : boxModel,
    reference : refModel,
    zone : zoneModel,
    creditMemo : creditMemoModel,
    account : accountModel,
    taxAg : taxAgModel
};

module.exports = Models;