var models = require("./models/mongo/models/models");


var getPluginData = (name) => {

	return new Promise((resolve, reject) => {

		models.plugin.findOne({ name : name }, (err , plugin) => {

			if(err){
				reject(err);
				return;
			}

			if(plugin && plugin.data) {
				resolve(plugin.data);
			} else {
				resolve({});
			}
		});

	});
}

//pluginId

var alert = (title, description, misc = {}, pluginId = null) => {

	return new Promise((resolve, reject) => {
		
		if(pluginId)
			misc.pluginId = pluginId;
		
		var entry = Object.assign({
			title : title,
			description
		}, misc)

		var a = new models.alert(entry);

		a.save((err) => {
			if(err){
				reject(err);
				return;
			}

			resolve(a);
		});
	});
}

var sendUnAuth = (res) => {
	res.status(401).send({ "Error" : "This endpoint requires authentication"});
}


module.exports = {
	models : models,
	getPluginData : getPluginData,
	alert,
	sendUnAuth
}