let express = require('express');
let router = express.Router();
let employee = require('../controller/employeeApi');

let employeeApi = new employee();
router.get('/employee_list', function(req, res) {
    employeeApi.employeeList().then((result)=>{
        res.send(result);
    },error=>{
        res.status(500).send(error);
    });
});


module.exports = router;