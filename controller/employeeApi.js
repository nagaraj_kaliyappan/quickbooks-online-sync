const models = require("../models/mongo/models/models");
const Employee = models.employee;

class employeeApi {
  constructor() {}

  saveEmployeeDetails(qresponse) {
    return new Promise((resolve, reject) => {
      let employee = new Array();
      employee = qresponse.Employee;
      let dbReq = [];
      employee.forEach((element, index) => {
        dbReq.push({
          id: element.Id,
          name: element.DisplayName,
          type: [element.domain],
          status: element.Active == "true" ? "ACTIVE" : "INACTIVE",
          createdAt: element.MetaData.CreateTime,
        });
        if (index == employee.length - 1) {
          Employee.insertMany(dbReq).then(
            (result) => {
              resolve(result);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });
    });
  }

  createORUpdateEmployeeDetails(response, reqId) {
    return new Promise((resolve, reject) => {
      let employee = response.Employee;
      if (reqId) {
        Employee.findOneAndUpdate(
          {
            _id: reqId,
          },
          {
            $set: {
              id: employee.Id,
              qb_id: employee.Id,
              name: employee.DisplayName,
              type: [employee.domain],
              status: employee.Active == "true" ? "ACTIVE" : "INACTIVE",
              createdAt: employee.MetaData.CreateTime,
            },
          }
        ).then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      } else {
        let dbReq = new Employee({
          id: employee.Id,
          qb_id: employee.Id,
          name: employee.DisplayName,
          type: [employee.domain],
          status: employee.Active == "true" ? "ACTIVE" : "INACTIVE",
          createdAt: employee.MetaData.CreateTime,
        });
        dbReq.save().then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      }
    });
  }

  employeeList() {
    return new Promise((resolve, reject) => {
      Employee.find({}).exec((err, employee) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(employee);
        }
      });
    });
  }
}

module.exports = employeeApi;
