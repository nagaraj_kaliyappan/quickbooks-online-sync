let models = require("../models/mongo/models/models");
let Customer = models.customer;

class customerApi {
  constructor() {}

  saveCustomerDetails(qresponse) {
    return new Promise((resolve, reject) => {
      let customer = new Array();
      customer = qresponse.Customer;
      let dbReq = [];
      customer.forEach((element, index) => {
        dbReq.push({
          id: element.Id,
          name: element.DisplayName,
          contactNo: element.PrimaryPhone
            ? element.PrimaryPhone.FreeFormNumber
            : "1234567890",
          email: element.PrimaryEmailAddr
            ? element.PrimaryEmailAddr.Address
            : "test@test.com",
          status: element.Active == "true" ? "ACTIVE" : "INACTIVE",
          value: Number(element.Balance),
          createdAt: element.MetaData.CreateTime,
        });
        if (index == customer.length - 1) {
          Customer.insertMany(dbReq).then(
            (result) => {
              resolve(result);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });
    });
  }

  createORUpdateCustomerDetails(response, reqId) {
    return new Promise((resolve, reject) => {
      let customer = response.Customer;
      if (reqId) {
        Customer.findOneAndUpdate(
          {
            _id: reqId,
          },
          {
            $set: {
              id: customer.Id,
              qb_id: customer.Id,
              name: customer.DisplayName,
              contactNo: customer.PrimaryPhone
                ? customer.PrimaryPhone.FreeFormNumber
                : "1234567890",
              email: customer.PrimaryEmailAddr
                ? customer.PrimaryEmailAddr.Address
                : "test@test.com",
              status: customer.Active == "true" ? "ACTIVE" : "INACTIVE",
              value: Number(customer.Balance),
              createdAt: customer.MetaData.CreateTime,
            },
          }
        ).then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      } else {
        let dbReq = new Customer({
          id: customer.Id,
          qb_id: customer.Id,
          name: customer.DisplayName,
          contactNo: customer.PrimaryPhone
            ? customer.PrimaryPhone.FreeFormNumber
            : "1234567890",
          email: customer.PrimaryEmailAddr
            ? customer.PrimaryEmailAddr.Address
            : "test@test.com",
          status: customer.Active == "true" ? "ACTIVE" : "INACTIVE",
          value: Number(customer.Balance),
          createdAt: customer.MetaData.CreateTime,
        });
        dbReq.save().then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      }
    });
  }

  customerList() {
    return new Promise((resolve, reject) => {
      Customer.find({}).exec((err, customer) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(customer);
        }
      });
    });
  }
}

module.exports = customerApi;
