const models = require("../models/mongo/models/models");
const Vendor = models.vendor;

class vendorApi {
  constructor() {}

  saveVendorDetails(qresponse) {
    return new Promise((resolve, reject) => {
      let vendor = new Array();
      vendor = qresponse.Vendor;
      let dbReq = [];
      vendor.forEach((element, index) => {
        dbReq.push({
          id: element.Id,
          name: element.DisplayName,
          repName: element.PrintOnCheckName,
          contactNo: element.PrimaryPhone
            ? element.PrimaryPhone.FreeFormNumber
            : "1234567890",
          email: element.PrimaryEmailAddr
            ? element.PrimaryEmailAddr.Address
            : "test@test.com",
          lastPurchase: element.MetaData.LastUpdatedTime,
          accountsPayable: Number(element.Balance),
          status: element.Active == "true" ? "ACTIVE" : "INACTIVE",
          createdAt: element.MetaData.CreateTime,
        });
        if (index == vendor.length - 1) {
          Vendor.insertMany(dbReq).then(
            (result) => {
              resolve(result);
            },
            (err) => {
              reject(err);
            }
          );
        }
      });
    });
  }

  createORUpdateVendorDetails(response, reqId) {
    return new Promise((resolve, reject) => {
      let vendor = response.Vendor;
      if (reqId) {
        Vendor.findOneAndUpdate(
          {
            _id: reqId,
          },
          {
            $set: {
              id: vendor.Id,
              qb_id: vendor.Id,
              name: vendor.DisplayName,
          repName: vendor.PrintOnCheckName,
          contactNo: vendor.PrimaryPhone
            ? vendor.PrimaryPhone.FreeFormNumber
            : "1234567890",
          email: vendor.PrimaryEmailAddr
            ? vendor.PrimaryEmailAddr.Address
            : "test@test.com",
          lastPurchase: vendor.MetaData.LastUpdatedTime,
          accountsPayable: Number(vendor.Balance),
          status: vendor.Active == "true" ? "ACTIVE" : "INACTIVE",
          createdAt: vendor.MetaData.CreateTime,
            },
          }
        ).then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      } else {
        let dbReq = new Vendor({
          id: vendor.Id,
          qb_id: vendor.Id,
          name: vendor.DisplayName,
          repName: vendor.PrintOnCheckName,
          contactNo: vendor.PrimaryPhone
            ? vendor.PrimaryPhone.FreeFormNumber
            : "1234567890",
          email: vendor.PrimaryEmailAddr
            ? vendor.PrimaryEmailAddr.Address
            : "test@test.com",
          lastPurchase: vendor.MetaData.LastUpdatedTime,
          accountsPayable: Number(vendor.Balance),
          status: vendor.Active == "true" ? "ACTIVE" : "INACTIVE",
          createdAt: vendor.MetaData.CreateTime,
        });
        dbReq.save().then(
          (result) => {
            resolve(result);
          },
          (err) => {
            reject(err);
          }
        );
      }
    });
  }

  vendorList() {
    return new Promise((resolve, reject) => {
      Vendor.find({}).exec((err, vendor) => {
        if (err) {
          reject(err);
          return;
        } else {
          resolve(vendor);
        }
      });
    });
  }
}

module.exports = vendorApi;
