let bodyParser = require("body-parser");
let sdk = require("./sdk/tools");
let util = require("./util");
let tutil = require("./tax_util");

const routes = require("express").Router();

let Router = require("./router/index");
const OAuthClient = require("intuit-oauth");
let oauth2_token_json = null;
let oauthClient = null;
const pageSize = 10;

let customerFn = require("./controller/customerApi");
let employeeFn = require("./controller/employeeApi");
let invoiceFn = require("./controller/invoiceApi");
let inventoryFn = require("./controller/inventoryApi");
let vendorFn = require("./controller/vendorApi");
let rev_tool = require("./reverse_util");


util.initializeClient().then(client => {
    oauthClient = client;
    const authUri = util.getAuthURL(oauthClient);
    util.alertUserAuth(authUri).then(() => console.log("Alert sent", authUri));
});

routes.use(bodyParser.json({ limit: "25mb" }));
routes.use(bodyParser.urlencoded({ extended: false }));

const urlencodedParser = bodyParser.urlencoded({ extended: false });

routes.get("/", (req, res) => {
    res.status(200).send("DHUB: Quickbooks Online!");
});


if (process.argv.indexOf("dev") !== -1) {
    routes.use((req, res, next) => {
        console.log("Setting test handler");
        req.owner = "TEST";
        next();
    });
}


setInterval(() => {

    if (oauth2_token_json != null) {
        oauthClient
            .refresh()
            .then(function(authResponse) {
                console.log('Tokens refreshed ');
            })
            .catch(function(e) {
                console.error('The error message is :' + e.originalMessage);
                console.error(e.intuit_tid);
            });
    }

}, 10 * 60 * 1000);

routes.get("/callback", async function(req, res) {

    await sdk.models.alert.remove({ id: 6000 }).exec();

    oauthClient
        .createToken(req.url)
        .then(async function(authResponse) {

            oauth2_token_json = JSON.stringify(authResponse.getJson(), null, 2);

            await util.syncAccount(oauthClient);
            await util.syncCOGS(oauthClient);
            await util.syncSPI(oauthClient);
            await util.syncDepositAccount(oauthClient);

            const url =
                oauthClient.environment == "sandbox" ?
                OAuthClient.environment.sandbox :
                OAuthClient.environment.production;


            await tutil.checkLocalAgs(oauthClient, url);
        })
        .catch(function(e) {
            console.error(e);
        });

    res.send(`<h1>Authentication complete</h1>
     <p>You may close this window now.</p>`)
});

routes.get("/check_accounts", async function(req, res) {

    try {
        await util.syncAccount(oauthClient);
        await util.syncCOGS(oauthClient);
        await util.syncSPI(oauthClient);
        await util.syncDepositAccount(oauthClient);

        const url =
            oauthClient.environment == "sandbox" ?
            OAuthClient.environment.sandbox :
            OAuthClient.environment.production;


        await tutil.checkLocalAgs(oauthClient, url);

        res.json({ "message": "done" })
    } catch (e) {
        res.json({ error: e })
    }

});





routes.get("/employee", async function(req, res) {
    try {
        let totalNoOfPages = await util.getTotalNoOfPages(
            oauthClient,
            "Employee",
            pageSize
        );
        for (let page = 0; page < totalNoOfPages; page++) {
            await getEmployees(page);
        }
        res.json({
            message: "success",
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getEmployees(page_number) {
    try {
        let query = await util.queryBuilder("Employee", page_number, pageSize);
        var authResponse = await util.performRequest(oauthClient, query);

        let response = authResponse.json;
        let qresponse = response.QueryResponse;
        let employeeApi = new employeeFn();
        await employeeApi.saveEmployeeDetails(qresponse);

        return {
            message: "success",
        };
    } catch (e) {
        return {
            message: e,
        };
    }
}

routes.get("/invoice", async function(req, res) {
    try {
        let totalNoOfPages = await util.getTotalNoOfPages(
            oauthClient,
            "Invoice",
            pageSize
        );
        for (let page = 0; page < totalNoOfPages; page++) {
            await getInvoices(page);
        }
        res.json({
            message: "success",
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getInvoices(page_number) {
    try {
        let query = await util.queryBuilder("Invoice", page_number, pageSize);
        var authResponse = await util.performRequest(oauthClient, query);

        let response = authResponse.json;
        let qresponse = response.QueryResponse;
        let invoiceApi = new invoiceFn();
        await invoiceApi.saveInvoiceDetails(qresponse);

        return {
            message: "success",
        };
    } catch (e) {
        return {
            message: e,
        };
    }
}

routes.get("/customer", async function(req, res) {
    try {
        let totalNoOfPages = await util.getTotalNoOfPages(
            oauthClient,
            "Customer",
            pageSize
        );
        for (let page = 0; page < totalNoOfPages; page++) {
            await getCustomers(page);
        }
        res.json({
            message: "success",
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getCustomers(page_number) {
    try {
        let query = await util.queryBuilder("Customer", page_number, pageSize);
        var authResponse = await util.performRequest(oauthClient, query);

        let response = authResponse.json;
        let qresponse = response.QueryResponse;
        let customerApi = new customerFn();
        await customerApi.saveCustomerDetails(qresponse);

        return {
            message: "success",
        };
    } catch (e) {
        return {
            message: e,
        };
    }
}

routes.get("/vendor", async function(req, res) {
    try {
        let totalNoOfPages = await util.getTotalNoOfPages(
            oauthClient,
            "Vendor",
            pageSize
        );
        for (let page = 0; page < totalNoOfPages; page++) {
            await getVendors(page);
        }
        res.json({
            message: "success",
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getVendors(page_number) {
    try {
        let query = await util.queryBuilder("Vendor", page_number, pageSize);
        var authResponse = await util.performRequest(oauthClient, query);

        let response = authResponse.json;
        let qresponse = response.QueryResponse;
        let vendorApi = new vendorFn();
        await vendorApi.saveVendorDetails(qresponse);

        return {
            message: "success",
        };
    } catch (e) {
        return {
            message: e,
        };
    }
}

routes.get("/inventory", async function(req, res) {
    try {
        let totalNoOfPages = await util.getTotalNoOfPages(
            oauthClient,
            "Item",
            pageSize
        );
        for (let page = 0; page < totalNoOfPages; page++) {
            await getInventory(page);
        }
        res.json({
            message: "success",
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

async function getInventory(page_number) {
    try {
        let query = await util.queryBuilder("Item", page_number, pageSize);
        var authResponse = await util.performRequest(oauthClient, query);

        let response = authResponse.json;
        let qresponse = response.QueryResponse;
        let inventoryApi = new inventoryFn();
        await inventoryApi.saveInventoryDetails(qresponse);

        return {
            message: "success",
        };
    } catch (e) {
        return {
            message: e,
        };
    }
}

routes.post("/customer/create", async function(req, res) {

    // Check if request is
    // authorized
    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    try {
        var authResponse = await util.addRecord(oauthClient, '/customer', req.body);

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        console.log(e);
        res.status(500).send(e);
    }
});

routes.post("/taxrate/create", async function(req, res) {

    // Check if request is
    // authorized
    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    try {

        req.body.TaxRateDetails = req.body.TaxRateDetails.map(x => {
            delete x.lid;
            return x;
        });

        var authResponse = await util.addRecord(oauthClient, '/taxservice/taxcode', req.body);

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        console.log(e);
        res.status(500).send(e);
    }
});


routes.post("/inventory/create", async function(req, res) {

    // Check if request is
    // authorized
    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    try {
        var authResponse = await util.addRecord(oauthClient, '/item', req.body);

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        console.log(e);
        res.status(500).send(e);
    }
});


routes.post("/employee/create", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }


    try {
        var authResponse = await util.addRecord(oauthClient, '/employee', req.body);

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }
});



routes.post("update_tax", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

});


routes.post("/vendor/create", async function(req, res) {


    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }


    try {
        var authResponse = await util.addRecord(oauthClient, '/vendor', req.body);

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }
});


routes.post("/invoice/create", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }


    try {
        var authResponse = await util.addRecord(oauthClient, '/invoice?minorversion=55', req.body);

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }
});


// Update functions

routes.put("/customer", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }


    try {
        var authResponse = await util.updateRecord(oauthClient, '/customer', req.body, "Customer");

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

routes.put("/employee", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }


    try {

        var authResponse = await util.updateRecord(oauthClient, '/employee', req.body, "Employee");

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

routes.put("/vendor", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    try {
        var authResponse = await util.updateRecord(oauthClient, '/vendor', req.body, "Vendor");

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        console.log(e);
        res.status(500).send(e);
    }
});


routes.put("/inventory", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    try {

        var authResponse = await util.updateRecord(oauthClient, '/item', req.body, "Item");

        let response = authResponse.json;
        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }
});

routes.put("/invoice", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }


    try {
        var authResponse = await util.updateRecord(oauthClient, '/invoice', req.body, "Invoice");

        let response = authResponse.json;

        res.json({
            message: "success",
            response
        });
    } catch (e) {
        res.status(500).send(e);
    }

});

routes.get("/download/:resName", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    var data = await rev_tool.fetchData(oauthClient, req.params.resName, req.query.page)
    res.json(data)
})

routes.get("/page_count/:resName", async function(req, res) {

    if (!req.owner) {
        sdk.sendUnAuth(res);
        return;
    }

    var data = await rev_tool.totalPage(oauthClient, req.params.resName)
    res.json({ count: data })
})



routes.use(
    "/api",
    Router.api,
    Router.customer,
    Router.employee,
    Router.vendor,
    Router.inventory,
    Router.invoice
);

module.exports = routes;