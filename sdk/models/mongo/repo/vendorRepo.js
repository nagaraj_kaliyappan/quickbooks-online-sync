let mongoose = require('mongoose');
let fn = require('../../../lib/common-utils/functions');
let deferred = require('../../../lib/common-utils/deferred');
let moment = require('moment');
let Vendor = require('../models/models').vendor;

class VendorRepo {

    findVendorById(id) {
        let qry = {
            _id: id,
            isDeleted: false
        };
        let leanObj = Vendor.findOne(qry, {__v: 0}).lean();
        return fn.defer(leanObj.exec, leanObj)();
    }

    findAllVendors() {
        let qry = {
            isDeleted: false
        };
        let leanObj = Vendor.find(qry, {__v: 0}).lean();
        return fn.defer(leanObj.exec, leanObj)();
    }

    deactivateVendorById(id) {
        return fn.defer(Vendor.update, Vendor)({
            _id: id
        }, {status: ''});
    }


    createVendor(data) {
        return fn.defer(Vendor.create, Vendor)(data);
    }

    deleteVendorById(id) {
        return fn.defer(Vendor.update, Vendor)({
            _id: id
        }, {$set: {isDeleted: true}});
    }

    updateVendor(id, data) {
        let update = {
            $set: data
        };
        return fn.defer(Vendor.update, Vendor)({
            _id: id
        }, update);
    }

}

let instance;

exports.getInstance = function () {
    if (!instance) instance = new VendorRepo();
    return instance;
};