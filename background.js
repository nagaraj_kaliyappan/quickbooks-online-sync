/* 
 Code executes in background
 This module will perform API requests to the quickbooks
 server.
 */
process.chdir(__dirname);
require("./sdk/init")();
let axios = require("axios");
let sdk = require("./sdk/tools");
let config = {
	dba : "testDB"
}

let tutil = require("./tax_util");

let dutil = require("./data_util");

let models = sdk.models;
var QuickBooksServer;
var devMode = true;

if(process.argv.indexOf("dev") == -1){
    config = require("../controller/config");
    devMode = false;
}


async function syncApis(){

	var pluginSettings = await sdk.getPluginData("QuickBooks Online");

	QuickBooksServer = axios.create({
	  baseURL:  devMode ? "http://localhost:8000/"  : `https://jtd.gophersauce.com/api_ext/`,
	  timeout: 100000,
	  headers: {
	  	'content-type' : 'application/json',
	  	'token' : devMode ? "TEST" : pluginSettings.user_token
	  }
	});


	var endPoints = [
		"vendor",
		"inventory",
		"customer",
		"employee",
		"invoice",		
	]
	
	//return;

	var taxReqs = await tutil.makeReq();

	if(taxReqs){
		
		for (var o = taxReqs.length - 1; o >= 0; o--) {
			var taxReq = await tutil.makeLatest(taxReqs[o].lid)
			console.log(taxReq)

			var res = await QuickBooksServer.post(`taxrate/create`, taxReq);
		
			var data = res.data.response.TaxRateDetails;
			console.log(data)

			for (var i = data.length - 1; i >= 0; i--) {
				var row = data[i];

				await models.taxRate.findOneAndUpdate({
					name : row.TaxRateName
				},{
					$set : {
						previous : row.RateValue,
						qb_id : row.TaxRateId,
						tax_code : res.data.response.TaxCodeId
					}
				}).exec();
			}

		}

		
	}
	
	for (var i = 0;i < endPoints.length;i++) {
		var e = endPoints[i];
		try {
			await processSec(e);
		} catch(ex){
			console.log(ex);
		}
	}

	

	
	return true;
}

async function processSec(endpoint){
		console.log("Processing ", endpoint)

		var items = await models[endpoint].find()
		.exec();

		for (var i = items.length - 1; i >= 0; i--) {
			var item = await formatData(endpoint, items[i]);

			if(!item)
				continue;

			if(items[i].qb_id){
				item.qb_id = items[i].qb_id;
				//console.log("Updating ", item)
				try{
                    var res = await QuickBooksServer.put(`${endpoint}`, item);
                    console.log(res.data);
                } catch(e){
                    console.log(e);
                }
				
				await dutil.sleep(12);
				continue;
			}
			
			
			console.log(item)
			

			try {
				var res = await QuickBooksServer.post(`${endpoint}/create`, item);
				var response = res.data.response;

				if(!response)
					continue;

				console.log(response);

			} catch (e){
				console.log("Error with ", endpoint)
				console.log(e);
			}

		

			var model_name = Object.keys(response)[0];

			console.log("Sync ID ", endpoint, response[model_name].Id);

	        await sdk.models[model_name.toLowerCase()].findOneAndUpdate({
	          _id : `${items[i]._id}`
	        },{
	          $set : {
	            qb_id : response[model_name].Id,
	            saved : true
	          }
	        }).exec();

	        await dutil.sleep(12)


		}	
}

async function formatData(e, item){
	var result;

	switch(e){
		case 'vendor':
			result = dutil.formatVendor(item);
		break;
		case 'inventory':
			result = await dutil.formatInventory(item, models.vendor, models.account);
		break;
		case 'employee':
			result = dutil.formatEmployee(item);
		break;
		case 'customer':
			result = await dutil.formatCustomer(item);
		break;
		case 'invoice':
			result = await dutil.formatInvoice(item, models.customer, models.inventory, models.account);
		break;
		default :
			result = item;
		break;
	}

	return result;
}

syncApis()
.then(() => {
	process.exit(0);
})