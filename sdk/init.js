var fs = require('fs');
const execSync = require('child_process').execSync;


var init = () => {

	var dir = './node_modules';

	if (!fs.existsSync(dir)){
	    console.log("Installing dependencies")
	    execSync("npm i")
	    process.exit(0);
	}
}

module.exports = init;