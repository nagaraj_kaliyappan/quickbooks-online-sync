"use strict";
let mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost:27017/dhub_qbonline_db';
mongoose.connect(mongoDB, { useUnifiedTopology: true,useNewUrlParser: true });
var connection = mongoose.connection;
connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
connection.on('open', function () {
    console.log("successfully connected to mongodb: ");
});
module.exports = {
    connection: connection
};

