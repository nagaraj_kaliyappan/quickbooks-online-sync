require("./init")();

const zipFolder = require('zip-a-folder');
const execSync = require('child_process').execSync;
const fs = require('fs'); 


class ZipAFolder {
 
    static main() {
        zipFolder.zipFolder('./', './archive.zip', function(err) {
            if(err) {
                console.log('Something went wrong!', err);
                return;
            }

            console.log("Archive saved");
        });
    }
}
 
execSync("rm -rf ./node_modules")

setTimeout(() => {
	

	ZipAFolder.main();

}, 1000 * 10)
