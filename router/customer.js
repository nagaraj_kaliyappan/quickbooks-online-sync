let express = require('express');
let router = express.Router();
let customer = require('../controller/customerApi');

let customerApi = new customer();

router.get('/customer_list', function(req, res) {
    customerApi.customerList().then((result)=>{
        res.send(result);
    },error=>{
        res.status(500).send(error);
    });
});




module.exports = router;