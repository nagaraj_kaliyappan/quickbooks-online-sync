let router = require('express').Router();
let customer = require('./customer');
let employee = require("./employee");
let inventory = require("./inventory");
let invoice = require("./invoice");
let vendor = require("./vendor");



module.exports = {
    api: router,
    customer: customer,
    employee : employee,
    inventory : inventory,
    invoice : invoice,
    vendor : vendor
};
