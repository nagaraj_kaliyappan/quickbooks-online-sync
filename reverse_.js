/* 
 Code executes in background
 This module will perform API requests to the quickbooks
 server.
 */
process.chdir(__dirname);
require("./sdk/init")();
let axios = require("axios");
let sdk = require("./sdk/tools");
let config = {
    dba: "testDB"
}

let rutil = require("./reverse_util")
let util = require("./util")

let models = sdk.models;
var QuickBooksServer;
var devMode = true;

if (process.argv.indexOf("dev") == -1) {
    config = require("../controller/config");
    devMode = false;
}


async function syncApis() {

    var pluginSettings = await sdk.getPluginData("QuickBooks Online");

    QuickBooksServer = axios.create({
        baseURL: devMode ? "http://localhost:8000/" : `https://jtd.gophersauce.com/api_ext/`,
        timeout: 100000,
        headers: {
            'content-type': 'application/json',
            'token': devMode ? "TEST" : pluginSettings.user_token
        }
    });

    var accounts = await rutil.get_accounts(QuickBooksServer)
    await rutil.get_cogs(QuickBooksServer, accounts)
    await rutil.get_itemAccount(QuickBooksServer, accounts)
    await rutil.get_sopi(QuickBooksServer, accounts)




    var endPoints = [
        "vendor",
        "item",
        "customer",
        "taxrate",
        "taxag"
    ]

    for (var i = 0; i < endPoints.length; i++) {
        var e = endPoints[i];
        try {
            await processSec(e);
        } catch (ex) {
            console.log(ex);
        }
    }

    return true;
}

async function processSec(res) {
    console.log("Processing ", res)

    var fnName = `get_${res}`;

    console.log(fnName)

    await rutil[fnName](QuickBooksServer)


}


syncApis()
    .then(() => {
        process.exit(0);
    })