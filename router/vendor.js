let express = require('express');
let router = express.Router();
let vendor = require('../controller/vendorApi');

let vendorApi = new vendor();
router.get('/vendor_list', function(req, res) {
    vendorApi.vendorList().then((result)=>{
        res.send(result);
    },error=>{
        res.status(500).send(error);
    });
});

module.exports = router;