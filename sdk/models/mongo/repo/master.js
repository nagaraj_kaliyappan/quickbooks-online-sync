let authRepo = require('./authRepo').getInstance();
let userRepo = require('./userRepo').getInstance();
let vendorRepo = require('./vendorRepo').getInstance();

let Repos = {
    auth: authRepo,
    user: userRepo,
    vendor: vendorRepo
};

module.exports = Repos;