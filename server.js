const express = require("express");
const path = require("path");
const handler = require("./handler");
let bodyParser = require("body-parser");

const app = express();
const port = process.env.PORT || "8000";


app.use(bodyParser.json({ limit: "25mb" }));
app.use(bodyParser.urlencoded({ extended: false }));

const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(handler);

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});
