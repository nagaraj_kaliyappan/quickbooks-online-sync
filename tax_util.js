var models = require("./sdk/tools").models;

async function addAgency(oauthClient, second = false, name = null, url = null) {


    var req = {
        "DisplayName": name
    };

    try {

        var response;
        const companyID = oauthClient.getToken().realmId;


        response = await oauthClient
            .makeApiCall({
                url: `${url}v3/company/${companyID}/taxagency`, ///1234/item
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(req),
            })

        var tx = response.json.TaxAgency;
        var dbEnt;


        dbEnt = new models.taxAg({
            id: name,
            qb_id: tx.Id
        });



        await dbEnt.save();

        return;

    } catch (e) {
        console.log(e);
        return;
    }
}


async function checkLocalAgs(oauthClient, url) {

    var taxRates = await models.taxRate.find()
        .exec();

    for (var i = taxRates.length - 1; i >= 0; i--) {
        var r = taxRates[i];
        var vid = r.name;

        if (r.parent && r.parent != "")
            continue;

        var currentAg = await models.taxAg.findOne({
            id: vid
        }).exec();

        if (!currentAg) {

            await addAgency(oauthClient, true, vid, url);

        }
    }


}

async function format(rate) {

    var agency = await models.taxAg.findOne({
        id: rate.name
    }).exec();

    if (!agency) {

        var parent = await models.taxRate.findOne({
            _id: rate.parent
        }).exec();

        agency = await models.taxAg.findOne({
            id: parent.name
        }).exec();

    }

    if (!agency)
        return null;

    var result = {
        "RateValue": `${rate.percentage}`,
        "TaxApplicableOn": "Sales",
        "TaxAgencyId": agency.qb_id,
        "TaxRateName": rate.name,
        lid: rate.id
    };

    if (rate.qb_id) {
        result["TaxRateId"] = rate.qb_id;
    }

    return result;
}

function makeTaxCode(list) {

    var names = [];

    for (var i = 0; i < list.length; i++) {

        names.push(list[i].name);
    }

    return names.join(" ");

}

async function makeLatest(id) {
    console.log("q", id)
    var r = await models.taxRate.findOne({
        name: id
    }).exec();

    var fr = await format(r);


    var parent = await models.taxRate.findOne({
        _id: r.parent
    }).exec();

    var pr = await format(parent)

    var taxRateList = [pr, fr];
    var taxCode = makeTaxCode([parent, r])

    var result = {
        "TaxRateDetails": taxRateList,
        "TaxCode": taxCode,
    }

    return result;
}

async function makeReq() {

    var rates = await models.taxRate.find({
        tax_code : null
    }).exec();

    var results = [];


    for (var i = rates.length - 1; i >= 0; i--) {
        var r = rates[i];

        var fr = await format(r);

        if (!fr)
            continue;

        if (!r.parent || r.parent == "")
            continue;

        var parent = await models.taxRate.findOne({
            _id: r.parent
        }).exec();

        var pr = await format(parent)

        if (!pr)
            continue;

        var taxRateList = [pr, fr];
        var taxCode = makeTaxCode([parent, r])

        var result = {
            "TaxRateDetails": taxRateList,
            "TaxCode": taxCode,
            lid: r.name
        }

        results.push(result);
    }

    if (results.length == 0)
        return null;

    return results;
}

module.exports = {
    makeReq,
    addAgency,
    checkLocalAgs,
    makeLatest
}